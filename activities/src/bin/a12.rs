// Topic: Implementing functionality with the impl keyword
//
// Requirements:
// * Print the characteristics of a shipping box
// * Must include dimensions, weight, and color
//
// Notes:
// * Use a struct to encapsulate the box characteristics
// * Use an enum for the box color
// * Implement functionality on the box struct to create a new box
// * Implement functionality on the box struct to print the characteristics

enum Color {
    Cyan,
    Magenta,
    Yellow,
}

struct Dimensions {
    width: i32,
    length: i32,
    height: i32,
}

impl Dimensions {
    fn print ( &self ) {
        println!( "{:?} in. wide, {:?} in. long, {:?} in. high ", self.width, self.length, self.height );
    }
}

struct Box {
    color: Color,
    weight: f32,
    size: Dimensions,
}

impl Box {

    fn new( color: Color, weight: f32, size: Dimensions ) -> Self {
        Box {
            color,
            weight,
            size,
        }
    }

    fn print( &self ) {
        match self.color {
            Color::Cyan => println!("This box is Cyan colored."),
            Color::Magenta => println!("This box is Magenta colored."),
            Color::Yellow => println!("This box is Yellow colored."),
        }
        println!("This box weights {:?} ounces", self.weight );
        self.size.print();
    }
}

fn main() {
    let box01 = Box::new( Color::Magenta, 14.0, Dimensions { width: 1, length: 2, height: 3 } );
    box01.print();

}

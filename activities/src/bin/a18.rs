// Topic: Result
//
// Requirements:
// * Create an structure named `Adult` that represents a person aged 21 or older:
//   * The structure must contain the person's name and age
//   * Implement Debug print functionality using `derive`

#[derive(Debug)]
struct Adult {
    age: u8,
    name: String,
}

// * Implement a `new` function for the `Adult` structure that returns a Result:
//   * The Ok variant should csontain the initialized structure, but only
//     if the person is aged 21 or older
//   * The Err variant should contain a String (or &str) that explains why
//     the structure could not be created

impl Adult {
    fn new( age: u8, name: String ) -> Result<Self,String> {
        if age >= 21 {
            Ok(Adult{age,name})
        } else {
            Err(String::from("adults must be 21 or older"))
        }
    }
}


// * Instantiate two `Adult` structures:
//   * One should be aged under 21
//   * One should be 21 or over
// * Use `match` to print out a message for each `Adult`:
//   * For the Ok variant, print any message you want
//   * For the Err variant, print out the error message

fn main() {
    
    let adult1 = Adult::new( 40, String::from("Alice") );
    
    match adult1 {
        Ok(a) => println!( "{:?}", a),
        Err(msg) => println!( "{:?}", msg), 
    }
    
    let adult2 = Adult::new( 14, String::from("Bob") );
    
    match adult2 {
        Ok(a) => println!( "{:?}", a),
        Err(msg) => println!( "{:?}", msg), 
    }
    
}

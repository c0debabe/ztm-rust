// Topic: Option
//
// Requirements:
// * Print out the details of a student's locker assignment
// * Lockers use numbers and are optional for students
//
// Notes:
// * Use a struct containing the student's name and locker assignment
// * The locker assignment should use an Option<i32>

struct Student {
    name: String,
    locker: Option<i32>,
}

fn main() {
    let sophmores = vec![
        Student { name: String::from("Susan"), locker: Some(43) },
        Student { name: String::from("Robert"), locker: Some(44) },
        Student { name: String::from("Xavier"), locker: None },
    ];
    for student in sophmores {
        println!( "Name: {:?}", student.name );
        match student.locker {
            Some(num) => println!( "Locker: {:?}", num ),
            None => println!( "Locker: Not Assigned" ),
        }
    }
}

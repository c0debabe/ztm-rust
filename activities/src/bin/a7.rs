// Topic: Working with an enum
//
// Program requirements:
// * Prints the name of a color to the terminal
//
// Notes:
// * Use an enum with color names as variants
// * Use a function to print the color name
// * The function must use the enum as a parameter
// * Use a match expression to determine which color
//   name to print

enum Color {
    Cyan,
    Magenta,
    Yellow,
}

fn print_color( color: Color ) {
    match color {
        Color::Cyan => println!("Cyan"),
        Color::Magenta => println!("Magenta"),
        Color::Yellow => println!("Yellow")
    }
}

fn main() {
    let my_color = Color::Yellow;
    print_color( my_color );
}

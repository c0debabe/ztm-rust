// Topic: Organizing similar data using structs
//
// Requirements:
// * Print the flavor of a drink and it's fluid ounces
//
// Notes:
// * Use an enum to create different flavors of drinks
// * Use a struct to store drink flavor and fluid ounce information
// * Use a function to print out the drink flavor and ounces
// * Use a match expression to print the drink flavor

enum Flavor {
    Grape,
    Orange,
    Watermelon,
}

struct Drink {
    flavor: Flavor,
    ounces: i32
}

fn print_drink( drink: Drink ) {
    match drink.flavor {
        Flavor::Grape => println!("Drink Flavor:\tGrape"),
        Flavor::Orange => println!("Drink Flavor:\tOrange"),
        Flavor::Watermelon => println!("Drink Flavor:\tWatermelon"),
    }
    println!("Drink Size:\t {:?}", drink.ounces);
}

fn main() {
    let my_drink = Drink {
        flavor: Flavor::Grape,
        ounces: 32,
    };
    print_drink( my_drink );
}

// Topic: Ownership
//
// Requirements:
// * Print out the quantity and id number of a grocery item
//
// Notes:
// * Use a struct for the grocery item
// * Use two i32 fields for the quantity and id number
// * Create a function to display the quantity, with the struct as a parameter
// * Create a function to display the id number, with the struct as a parameter


struct GroceryItem {
    id: i32,
    qty: i32
}

fn print_qty( item: &GroceryItem ) {
    println!("Item Qty: {:?}", item.qty );
}

fn print_id( item: &GroceryItem ) {
    println!("Item ID: {:?}", item.id );
}

fn main() {
    let apple = GroceryItem { id: 23, qty: 200 };
    print_id( &apple );
    print_qty( &apple );
}

// Topic: Advanced match
//
// Requirements:
// * Print out a list of tickets and their information for an event
// * Tickets can be Backstage, Vip, and Standard
// * Backstage and Vip tickets include the ticket holder's name
// * All tickets include the price
//
// Notes:
// * Use an enum for the tickets with data associated with each variant
// * Create one of each ticket and place into a vector
// * Use a match expression while iterating the vector to print the ticket info

/*
We're now entering levels of exercises where they feel like a lot of work for 
nothing except that practicing this methods really does help with learning.
 - sign -
*/


// * Backstage and Vip tickets include the ticket holder's name

// * Tickets can be Backstage, Vip, and Standard
enum Ticket {
    Backstage(f32, String),
    VIP(f32,String),
    Standard(f32),
}

fn main() {
    // * Print out a list of tickets and their information for an event
    // Create tickets
    let mut tickets = vec![
        Ticket::Standard( 40.00 ),
        Ticket::Backstage( 80.00, String::from("Robin") ), 
        Ticket::VIP( 160.00, String::from("Ava") ), 
    ];
    // Print out tickets
    for t in &tickets {
        match t {
            Ticket::Standard(price) => println!( "Standard Ticket: ${:?}", price ),
            Ticket::Backstage(price,name)=> println!( "Backstage Ticket: ${:?} for {:?}", price, name ),
            Ticket::VIP( price,name ) => println!( "VIP Ticket: ${:?} for {:?}", price, name ),
        }
    }
}

// Topic: Basic arithmetic
//
// Program requirements:
// * Displays the result of the sum of two numbers
//
// Notes:
// * Use a function to add two numbers together
// * Use a function to display the result
// * Use the "{:?}" token in the println macro to display the result

fn sum( v1: i32, v2: i32 ) -> i32 {
    v1 + v2
}

fn show( data: i32 ) {
    println!("{:?}", data);
}

fn main() {
    show( sum( 4, 12 ) );
    show( sum( 5, 11 ) );
    show( sum( 6, 10 ) );
    show( sum( 7, 9 ) );
}

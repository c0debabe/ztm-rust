// Topic: Flow control using if..else if..else
//
// Program requirements:
// * Display ">5", "<5", or "=5" based on the value of a variable
//   is > 5, < 5, or == 5, respectively
//
// Notes:
// * Use a variable set to any integer value
// * Use an if..else if..else block to determine which message to display
// * Use the println macro to display messages to the terminal

fn check( v: i16 ) {
    if v < 5  {
        println!( "{:?} < 5", v );
    } else if v == 5 {
        println!( "{:?} = 5", v );
    } else {
        println!( "{:?} > 5", v );
    }
}

fn main() {
    // fancier than needed but :shrug:
    let mut i = 1;
    while i <= 10 {
        check( i );
        i = i + 1;
    }
    
}

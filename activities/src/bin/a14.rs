// Topic: Strings
//
// Requirements:
// * Print out the name and favorite colors of people aged 10 and under
//
// Notes:
// * Use a struct for a persons age, name, and favorite color
// * The color and name should be stored as a String
// * Create and store at least 3 people in a vector
// * Iterate through the vector using a for..in loop
// * Use an if expression to determine which person's info should be printed
// * The name and colors should be printed using a function


struct Person {
    name: String,
    age: i32,
    color: String,
}

impl Person {
    fn print( &self ) {
        println!("Name: {:?}", self.name);
        println!("Favorite Color: {:?}", self.color);
    }
}

fn main() {
    
    let mut people = Vec::new();
    people.push(Person {
        name: "John".to_owned(), // or String::from("John")
        age: 4,
        color: "Blue".to_owned(),
    });
    people.push(Person {
        name: "Susie".to_owned(),
        age: 12,
        color: "Purple".to_owned(),
    });
    people.push(Person {
        name: "Bobbie".to_owned(),
        age: 8,
        color: "Orange".to_owned(),
    });

   for person in &people {
       if person.age < 10 {
           person.print();
       }
   } 
    
}
